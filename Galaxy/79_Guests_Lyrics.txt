﻿Seventy-nine guests! 
They are here to bestow us this simple melody
Seventy-nine guests!
Their tune that can not be heard in its entirity

I won’t care! What they wanna say I’m gonna to sing it anyway
It goes la, di, dah
It goes one, two, three
It makes me want to dance
As it rises in me
It goes la, di, dah
It makes me want to dance between the speres….

Seventy-nine guests! 
And they all expect to be entertained
Seventy-nine guests!
At this celebration of a universe maintained

They want upbeat music, but in my heart is the song of creation
It goes la, di, dah
It goes one, two, three
It makes me want to dance
As it rises in me
It goes la, di, dah
It makes me want to dance between the speres….
It goes la, di, dah
It goes one, two, three
It makes me want to dance
As it rises in me
It goes la, di, dah
You’ll find me dancing to the music of the spheres

Seventy-nine guests!
Seventy-nine witnesses
Seventy-nine orbs each with a melody

Seventy-nine guests!
All of them sing songs of love
Seventy-nine guests, singing glory, glory, glory, glory evermore
